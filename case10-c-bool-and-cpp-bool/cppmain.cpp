#include <iostream>
using namespace std;

void func(bool a){cout<<"bool\n";}
void func(int a){cout<<"int\n";}

int main()
{
  func(false);
  func(0);
  return 0;
}
