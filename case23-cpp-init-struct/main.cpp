#include <stdio.h>

class test
{
public:
    virtual void func(){}
};

class test1 : public test
{
public:
    virtual void func(){printf("test1\n");}
};

class test2 : public test
{
public:
    virtual void func(){printf("test2\n");}
};


struct ttt
{
    int i;
    test* c;
};

ttt data[1]={{1,new test2()},{2,new test1()}};

int main()
{
    return 0;
}
