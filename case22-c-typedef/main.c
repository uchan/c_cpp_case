struct A
{
  char a;
};

struct B
{
  short a;
};

typedef struct B B;
typedef struct{int a;}A;

#include <stdio.h>

void main(void)
{
  printf("struct A:%zu\n",sizeof(struct A));
  printf("struct B:%zu\n",sizeof(struct B));
  printf("A:%zu\n",sizeof(A));
  printf("B:%zu\n",sizeof(B));
}
