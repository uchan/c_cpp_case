#include <stdio.h>

class test
{
public:
  static const int a;
  static const char p[];
};

const char test::p[]="aaa\n";
const int test::a=3;

int main()
{
  printf("%s", test::p);
  return 0;
}
