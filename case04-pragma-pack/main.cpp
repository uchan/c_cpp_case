#include <stdio.h>
#define UINT8 char

#pragma pack(1)
    struct Data{
UINT8  : 8;
        UINT8 acnManFlag : 1;
        UINT8 svlFlag : 1;
        UINT8 custFlag : 1;
        UINT8 prlUpdateFlag : 1;
        UINT8 kmpDiagFlag : 1;
        UINT8 usbDiagFlag : 1;
        UINT8 downloadFlag : 1;
        UINT8 pagingFlag : 1;
        UINT8 dataComFlag : 1;
        UINT8 voiceComFlag : 1;
        UINT8  : 6;
    }m_data;
    #pragma pack()


#pragma pack(1)
  struct bitfield
  {
    signed int a :3;
    unsigned int b :13;
    unsigned int c :1;
  };
#pragma pack()

int main()
{
  printf("size:%d\n", sizeof(Data));
  printf("size:%d\n", sizeof(bitfield));
  return 0;
}
