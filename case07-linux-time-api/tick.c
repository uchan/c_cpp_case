#include <stdio.h>
#include <time.h>
#include <unistd.h>

void main(void)
{
  struct timespec ts1;
  struct timespec ts2;
  clock_gettime(CLOCK_MONOTONIC,&ts1);
  sleep(3);
  clock_gettime(CLOCK_MONOTONIC,&ts2);
  printf("tv_sec:%ld\n",ts1.tv_sec-ts2.tv_sec);
  printf("tv_nsec:%ld\n",ts1.tv_nsec-ts2.tv_nsec);
}
