#include <iostream>
using namespace std;

class Test
{
public:
    Test(int i){cout<<"in class: "<<i<<endl;}
};

void func()
{
  Test a(1);
  cout<<"in func"<<endl;
  Test b(2);
}

int main()
{
  func();
  return 0;
}
