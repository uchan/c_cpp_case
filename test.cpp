#include <stdio.h>
#include <new>

struct A
{
  enum consts{off=0,on=1};
  struct B
  {
    enum consts{set1=10,set2=20};
    int a;
    char b;
  }data[10];
  double c;
}data;

int main1(void)
{
  struct A ttt;
  struct A::B yyy;
  int a=A::on;
  a=A::B::set2;
  return 0;
}

enum aaa{aa,bb};
int main2(void)
{
  aaa a;
  a=static_cast<aaa>(3);
  return 0;
}

struct tt{int a;}*pdata;

struct test3
{
int a;
void* operator new (std::size_t size)
{
  printf("in new");
  return operator new[](size);
}
};
int main3(void)
{
  test3 test;
  test3* p1=new test3;
  test3* p2=new test3[1];
  delete[] p1;
  delete[] p2;
  return 0;
}

namespace testsp
{
class test
{
public:
  void func();
};
}

namespace testsp
{
void test::func()
{
}
}
int main4(void)
{
  testsp::test t;
  t.func();
  return 0;
}
int main5(void)
{
  printf("wchar_t size:%l\n", sizeof(wchar_t));
  return 0;
}
int main6(void)
{
struct test{
 char b;
 int a:4;
};
  test tt;
  tt.a=100;
  tt.b='A';
  printf("main6:\na:%d\n",tt.a);
  printf("b:%d\n\n",tt.b);
  return 0;
}
enum main7enum{main7aaa=2,main7bbb=5};
int main7(){ main7enum eee=main7enum(9);
printf("main7: e:%d\n",eee);
return 0;}
int main(void)
{return main7();}
