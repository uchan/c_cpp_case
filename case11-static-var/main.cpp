#include <iostream>
#include "header.h"

using namespace std;

void func1();

int main()
{
  cout<<"main: header="<<header<<",addr="<<static_cast<void*>(&header)<<endl;
  func1();
  cout<<"main: header="<<header<<",addr="<<static_cast<void*>(&header)<<endl;
  return 0;
}
