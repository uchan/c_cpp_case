#include "func.h"
#include <stdio.h>

static void functest(void)
{
  printf("test call\n");
}

funcptr retfunc(void)
{
  return functest;
}

void callfunc(void)
{
  functest();
}
